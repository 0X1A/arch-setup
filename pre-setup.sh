#!/bin/bash

echo "Arch pre setup script"
sleep 1

echo "Running pacstrap to /mnt of base and base-devel..."
pacstrap /mnt base base-devel

echo "Creating fstab"
genfstab -U -p /mnt >> /mnt/etc/fstab

echo "You should be good to go..."
