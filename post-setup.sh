#!/usr/bin/env bash

NOCONF=" --noconfirm"

echo "Warning: This script is entirely specific to my usage"
echo "To customize your initramfs edit mkinitcpio.conf accordingly"
echo "For all options prompt's use numbers (i.e 1, 2, 3)"
sleep 3

set_locale() {
	echo en_US ISO-8859-1 >> /etc/locale.gen
	echo en_US.UTF-8 UTF-8 >> /etc/locale.gen
	locale-gen
	echo LANG=en_US.UTF-8 > /etc/locale.conf
	ln -s /usr/share/zoneinfo/America/Chicago /etc/localtime
	export LANG=en_US.UTF-8
}

set_mkinitcpio() {
	hwclock --systohc --utc
	cat mkinitcpio.conf > /etc/mkinitcpio.conf
	mkinitcpio -p linux
}

set_users_pass(){
	echo -e "\033[1m\033[31mEnter the root password"
	passwd
	echo -e "\033[1m\033[31mEnter user name:"
	read USR
	echo -e "\033[1m\033[31mEnter default user shell:"
	echo "1: bash"
	echo "2: fish"
	read _USHELL
	if [[ $_USHELL == "2" ]] ; then
		pacman -S fish $NOCONF
		useradd -m -g users -s /usr/bin/fish $USR
	else
		useradd -m -g users -s /bin/bash $USR
	fi
	echo -e "\033[1m\033[31mEnter user password:"
	passwd $USR
	usermod -aG wheel $USR
	cat sudoers > /etc/sudoers
	chown -c root:root /etc/sudoers
	chmod -c 0440 /etc/sudoers
}

set_drivers() {
	echo -e "\033[1m\033[31mEnter your GPU manufacturer:"
	echo -e "\033[1m\033[32m1: AMD"
	echo -e "\033[1m\033[32m2: Intel"
	echo -e "\033[1m\033[32m3: Nvidia"
	read GPU
	if [[ $GPU == "1" ]] ; then
		pacman -S xf86-video-ati $NOCONF
	elif [[ $GPU == "2" ]] ; then
		pacman -S xf86-video-intel $NOCONF
	elif [[ $GPU == "3" ]] ; then
		pacman -S xf86-video-nouveau $NOCONF
	fi
}

set_de() {
	pacman -S firefox vim hunspell hunspell-en gst-libav libreoffice-fresh networkmanager $NOCONF
	echo -e "\033[1m\033[31mEnter your DE choice:"
	echo -e "\033[1m\033[32m1: Gnome"
	echo -e "\033[1m\033[32m2: KDE"
	echo -e "\033[1m\033[32m3: Xfce"
	echo -e "\033[1m\033[32m4: Xorg-server only"
	echo -e "\033[1m\033[32mPress enter for none"
	# Install default stuff
	read DE
	if [[ $DE == "1" ]] ; then
		pacman -S xorg-server gnome gedit file-roller gnome-boxes rhythmbox transmission-gtk gnome-tweak-tool $NOCONF
		systemctl enable gdm
	elif [[ $DE == "2" ]] ; then
		pacman -S xorg-server plasma kde-applications ktorrent $NOCONF
		systemctl enable sddm
	elif [[ $DE == "3" ]] ; then
		pacman -S xorg-server xfce $NOCONF
	elif [[ $DE == "4" ]] ; then
		pacman -S xorg-server xorg-server-utils xorg-xinit $NOCONF
	fi
	systemctl enable NetworkManager
}

plat() {
	echo -e "\033[1m\033[31mIs this a laptop? (For non-DE installation select yes)"
	echo -e "\033[1m\033[31m1: Yes"
	echo -e "\033[1m\033[31mPress enter for No"
	read PLAT
	if [[ $PLAT == "1" ]] ; then
		pacman -S wireless_tools wpa_supplicant xf86-input-synaptics wpa_actiond dialog $NOCONF
	fi
}

set_hostname() {
	echo -e "\033[1m\033[31mEnter your desired hostname"
	read _HOSTN
	echo "$_HOSTN" > /etc/hostname
}

set_bootloader() {
	lsblk
	echo "Above are the available disks. Enter disk to install bootloader onto (/dev/sdX):"
	read DISK
	echo "UEFI or BIOS? (1: UEFI, 2: BIOS):"
	read MODE
	pacman -S grub-bios $NOCONF
	if [[ $MODE == "1" ]] ; then
		pacman -S efibootmgr $NOCONF
		grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id="Arch Linux" --recheck
	elif [[ $MODE == "2" ]] ; then
		grub-install $DISK --recheck
	fi
	grub-mkconfig -o /boot/grub/grub.cfg
}

set_locale
set_mkinitcpio
set_users_pass
set_drivers
set_de
plat
set_hostname
set_bootloader
